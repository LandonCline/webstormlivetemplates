#SelectQuote Live Templates Repo

## Get Started
1. Create a directory for your template files. I have mine in `~/Projects/WebstormTemplates`
2. If you are on a MAC you can create a symbolic link to the template directories and for Windows you can create a shortcut.

    MAC: 
      - Directory for Live Templates - ~/Library/Preferences/WebStorm2016.3/settingsRepository/repository/templates
      - SymbolicLink `ln - s SOURCE TARGET`
    
    WINDOWS:

3. After symbolic link is created cd into directory and verify you are in correct directory.
4. `git clone` the repo.
5. Your live template XML files should now be there and you can use `git pull` when there are updates.
6. Restart Webstorm to see template changes.